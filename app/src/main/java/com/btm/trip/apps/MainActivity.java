package com.btm.trip.apps;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.btm.trip.apps.ui.fragment.HelpFragment;
import com.btm.trip.apps.ui.fragment.HistoryFragment;
import com.btm.trip.apps.ui.fragment.HomeFragment;
import com.btm.trip.apps.ui.fragment.ProfileFragment;
import com.btm.trip.apps.ui.fragment.ReminderFragment;
import com.btm.trip.apps.ui.fragment.TripFragment;

/**
 * Created by user on 12/1/2015.
 */
public class MainActivity extends AppCompatActivity implements NavMenuFragment.FragmentDrawerListener {
    private Toolbar mToolbar;
    private NavMenuFragment drawerFragment;
    private ImageView backImageView;
    private TextView toolbarTitle;
    private ImageView toolbarImage;
    boolean isMainmenu = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_main_name_text_view);
        toolbarImage = (ImageView) mToolbar.findViewById(R.id.toolbar_mini_konsula_image_view);
        backImageView = (ImageView) findViewById(R.id.toolbar_main_setting_image_view);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do nothing
            }
        });

        // set home menu
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawerFragment = (NavMenuFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        //drawerFragment.setImage(userData.photo);
        displayView(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = "";
        switch (position) {
            case 0:
                toolbarImage.setVisibility(View.VISIBLE);
                backImageView.setVisibility(View.VISIBLE);
                fragment = new HomeFragment();
                isMainmenu = true;
                break;
            case 1:
                toolbarImage.setVisibility(View.INVISIBLE);
                backImageView.setVisibility(View.INVISIBLE);
                title = "Trip";
                fragment = new TripFragment();
                isMainmenu = false;


                break;
            case 2:
                toolbarImage.setVisibility(View.INVISIBLE);
                backImageView.setVisibility(View.INVISIBLE);
                title = "Reminder";
                fragment = new ReminderFragment();
                isMainmenu = false;
                break;
            case 3:
                toolbarImage.setVisibility(View.INVISIBLE);
                backImageView.setVisibility(View.INVISIBLE);
                title = "History";
                fragment = new HistoryFragment();
                isMainmenu = false;
                break;
            case 4:
                toolbarImage.setVisibility(View.INVISIBLE);
                backImageView.setVisibility(View.INVISIBLE);
                title = "Profile";
                fragment = new ProfileFragment();
                isMainmenu = false;
                break;
            case 5:
                toolbarImage.setVisibility(View.INVISIBLE);
                backImageView.setVisibility(View.INVISIBLE);
                title = "Help";
                fragment = new HelpFragment();
                isMainmenu = false;
                break;
            default:
                toolbarImage.setVisibility(View.VISIBLE);
                backImageView.setVisibility(View.VISIBLE);
                title = getString(R.string.app_name);
                fragment = new HomeFragment();
                isMainmenu = true;
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_tabcontent, fragment);
            fragmentTransaction.commit();
            toolbarTitle.setText(title);
        }
    }
}
