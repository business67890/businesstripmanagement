package com.btm.trip.apps.ui.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.btm.trip.apps.R;
import com.btm.trip.apps.adapter.ImageSlideAdapter;
import com.btm.trip.apps.model.HomeImageSliderModel;
import com.btm.trip.apps.networking.ApiUrl;
import com.btm.trip.apps.networking.CustomVolleyRequest;
import com.btm.trip.apps.singleton.Application;
import com.btm.trip.apps.ux.CirclePageIndicator;
import com.btm.trip.apps.ux.PageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konsula on 25/02/2016.
 */
public class HomeFragment extends Fragment {
    private LinearLayout BtnDirektori, BtnTeledokter, BtnChatDokter, BtnEstore;
    private String currentLanguage;
    private ProgressBar progressBar;
    private ViewPager viewPagerHeader;
//    private ImageView imageViewBanner;
    private Handler handler;
    private Runnable runnable;
    int position = 1;
    private Boolean hasloadbanner;
    private android.app.AlertDialog dialog;


    //slider
    private ViewPager mViewPager;
    List<HomeImageSliderModel> homeImageSliderList;
    PageIndicator mIndicator;
    boolean stopSliding = false;
    private Runnable animateViewPager;
    private static final long ANIM_VIEWPAGER_DELAY = 10000;

    //network
    ApiUrl apiUrl;
    String apiUrlContent;
    RequestQueue requestQueue;

    final String TAG = "HomeFragment";
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        // Inflate layout for this fragment
        final View parentview = inflater.inflate(R.layout.fragment_main_menu, container, false);

        BtnDirektori = (LinearLayout) parentview.findViewById(R.id.menu_direktori);
        BtnTeledokter = (LinearLayout) parentview.findViewById(R.id.menu_teledokter);
        BtnChatDokter = (LinearLayout) parentview.findViewById(R.id.menu_chatdokter);
        BtnEstore = (LinearLayout) parentview.findViewById(R.id.menu_estore);
        progressBar = (ProgressBar) parentview.findViewById(R.id.progressBar);
//        imageViewBanner = (ImageView) parentview.findViewById(R.id.image_banner);
        mViewPager = (ViewPager) parentview.findViewById(R.id.view_pager_slider);
        mIndicator = (CirclePageIndicator)parentview.findViewById(R.id.indicator_slider);

        apiUrl = Application.getInstanceApiUrl();

        progressBar.setProgress(20);
        progressBar.setSecondaryProgress(50);
        currentLanguage = getResources().getConfiguration().locale.getLanguage();
        currentLanguage = (currentLanguage.equals("en") || currentLanguage.equals("EN")) ? "en" : "id";
        BtnDirektori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                accessLocation();
            }
        });
        BtnTeledokter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getdataTeledocRetriction();
            }
        });
        BtnChatDokter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getdataChat();

            }
        });
        BtnEstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do nothing
            }
        });

        //homeBannerClone = new ArrayList<BannerHomeModel.Result>();
        //BannerHomeModel bannerHomeModelClone = new BannerHomeModel();

        //homeBannerClone.add(bannerHomeModelClone.results.get(0));
        //setupViewPagerHeader(viewPagerHeader, bannerHomeModelClone.results);

        setupImageSlider();

        return parentview;
    }

    void setupImageSlider(){
        try {
            apiUrlContent   = apiUrl.getUrl()+"/slider";
            tulisLog("apiUrlContent :" + apiUrlContent);
            CustomVolleyRequest cr = new CustomVolleyRequest(Request.Method.POST, apiUrlContent, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            tulisLog("slider response :" + response);
                            try {
                                JSONArray postsArray = response.getJSONArray("data");
                                tulisLog("postsArray.length() :" + postsArray.length());
                                if(postsArray.length() <=0 ){
                                    tulisLog("Data tidak ada");
                                }else {
                                    homeImageSliderList = new ArrayList<>();
                                    try{
                                        for (int i = 0; i < postsArray.length(); i++) {
                                            HomeImageSliderModel homeImageSliderModel = new HomeImageSliderModel();
                                            JSONObject json = postsArray.getJSONObject(i);
                                            String path = json.getString("path");
                                            String caption = json.getString("caption");
                                            String action = json.getString("action");


                                            homeImageSliderModel.setPath(path);
                                            homeImageSliderModel.setCaption(path);
                                            homeImageSliderModel.setAction(path);

                                            homeImageSliderList.add(homeImageSliderModel);
                                        }

                                        mViewPager.setAdapter(new ImageSlideAdapter(
                                                getActivity(), homeImageSliderList));
                                        mIndicator.setViewPager(mViewPager);
                                        runnable(homeImageSliderList.size());
                                        handler.postDelayed(animateViewPager,
                                                ANIM_VIEWPAGER_DELAY);

                                    }catch(Exception e){
                                        tulisLog(e.getMessage());
                                    }
                                }

                            } catch (Exception e) {
                                tulisLog(e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    tulisLog(error.getMessage());
                }
            });
            requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(cr);

        }catch(Exception e){
            tulisLog(e.getMessage());
        }
    }

    public void runnable(final int size) {
        handler = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!stopSliding) {
                    if (mViewPager.getCurrentItem() == size - 1) {
                        mViewPager.setCurrentItem(0);
                    } else {
                        mViewPager.setCurrentItem(
                        mViewPager.getCurrentItem() + 1, true);
                    }
                    handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
    }

    void tulisLog(String str){
        Log.d(TAG, str);
    }




}
