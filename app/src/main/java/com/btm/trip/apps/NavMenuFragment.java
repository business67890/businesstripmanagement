package com.btm.trip.apps;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.btm.trip.apps.model.CommonModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by Owner on 12/3/2015.
 */
public class NavMenuFragment extends Fragment {
    private static String TAG = NavMenuFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private static String[] navTitleMenu = null;
    private FragmentDrawerListener drawerListener;
    private TextView name_username;
    private TextView version;
    private TextView Server;
    private ImageView image_logo;
    private int test;

    private ArrayList<CommonModel> menuList = new ArrayList<CommonModel>();

    public NavMenuFragment() {
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set data
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "Home", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "Trip", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "Reminder", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "History", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "Profile", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
        menuList.add(createStaticObjectData(getActivity().getResources().getDrawable(R.drawable.ic_home_inactive), "Help", getActivity().getResources().getDrawable(R.drawable.ic_home_active)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_nav_menu, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.nav_menu_recycler_view);
        name_username = (TextView) view.findViewById(R.id.nav_welcome_username);
        image_logo = (ImageView) view.findViewById(R.id.nav_welcome_logo_image_view);
        version = (TextView) view.findViewById(R.id.nav_menu_version);
        Server = (TextView) view.findViewById(R.id.nav_menu_server);
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version.setText("V " + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            //Handle exception
        }
        adapter = new NavigationDrawerAdapter(getActivity(), menuList);
        adapter.setCurrentPosition(0);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mDrawerLayout.closeDrawer(containerView);
                test = position;
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        name_username.setText("Admin");
        /*if (userData.photo != null) {
            Picasso.with(getActivity())
                    .load(userData.photo)
                    .into(image_logo);
        }*/
        //Server.setText(AppConstant.DOMAIN_URL.equals("https://staging-api.konsula.com") ? "Staging" : "");

        return view;
    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                adapter.setCurrentPosition(test);
                drawerListener.onDrawerItemSelected(drawerView, test);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                //toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }

    // create object data
    private CommonModel createStaticObjectData(Drawable iconId, String title, Drawable activeIcon) {
        CommonModel menuModel = new CommonModel();
        menuModel.setProperty("icon", iconId);
        menuModel.setProperty("activeIcon", activeIcon);
        menuModel.setProperty("title", title);
        return menuModel;
    }

    // adapter
    public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {
        private List<CommonModel> listMenu = Collections.emptyList();
        private LayoutInflater inflater;
        private Context context;
        private SparseBooleanArray selectedItems = new SparseBooleanArray();
        private int pos = -1;

        public NavigationDrawerAdapter(Context context, List<CommonModel> listMenu) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.listMenu = listMenu;
        }

        public void delete(int position) {
            listMenu.remove(position);
            notifyItemRemoved(position);
        }

        private void setCurrentPosition(int position) {
            selectedItems.put(position, true);
            pos = position;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.item_nav_menu, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            CommonModel current = listMenu.get(position);
            holder.title.setText(current.getProperty("title"));

            Drawable icon = null;

            /*if (AppConstant.FROM_INDEX == position) {
                icon = (Drawable) current.getObject("activeIcon");
                holder.container.setSelected(true);
                holder.title.setSelected(true);
                holder.icon.setImageDrawable(icon);
            } else {*/
            icon = (Drawable) current.getObject("icon");
            holder.container.setSelected(false);
            holder.title.setSelected(false);
            holder.icon.setImageDrawable(icon);
            //}
        }

        @Override
        public int getItemCount() {
            return listMenu.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView title;
            ImageView icon;
            RelativeLayout container;

            public ViewHolder(View itemView) {
                super(itemView);
                container = (RelativeLayout) itemView.findViewById(R.id.item_nav_menu_container);
                title = (TextView) itemView.findViewById(R.id.item_nav_menu_name_text_view);
                icon = (ImageView) itemView.findViewById(R.id.item_nav_menu_icon_image_view);

                container.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                //        Save the selected positions to the SparseBooleanArray

                if (pos > -1) {
                    selectedItems.delete(pos);
                }
                if (!selectedItems.get(getAdapterPosition(), false)) {
                    setCurrentPosition(getAdapterPosition());
                }
            }
        }
    }
}
