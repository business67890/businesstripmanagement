package com.btm.trip.apps.model;

/**
 * Created by Ajib31 on 05/02/2017.
 */

public class HomeImageSliderModel {
    private String path;
    private String caption;
    private String action;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
