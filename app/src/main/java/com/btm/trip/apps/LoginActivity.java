package com.btm.trip.apps;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import jp.wasabeef.blurry.Blurry;

/**
 * Created by konsula on 2/25/2016.
 */
public class LoginActivity extends AppCompatActivity {

    private Button btn_login;
    private LinearLayout tvForgotPassword;
    private EditText emailTextView;
    private EditText passwordTextView;
    private ScrollView l_view;
    private RelativeLayout nav_relative_main;
    private ImageView nav_img_background;

    Runnable unpressRunnable;
    Runnable pressRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nav_relative_main = (RelativeLayout)findViewById(R.id.nav_relative_main);
        nav_img_background = (ImageView)findViewById(R.id.nav_img_background);
        btn_login = (Button)findViewById(R.id.btn_login);
        nav_img_background.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Blurry.with(LoginActivity.this)
                        .radius(20)
                        .sampling(3)
                        .color(Color.argb(16, 21, 126, 0))
                        .async()
                        .capture(nav_img_background)
                        .into(nav_img_background);
            }
        });


        //action button
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        pressRunnable = new Runnable() {
            @Override
            public void run() {
                btn_login.setPressed(true);
                btn_login.postOnAnimationDelayed(unpressRunnable, 250);
            }
        };

        unpressRunnable = new Runnable() {
            @Override
            public void run() {
                btn_login.setPressed(false);
                btn_login.postOnAnimationDelayed(pressRunnable, 250);
            }
        };

        initialButton();
    }

    public void initialButton() {
//        l_view = (ScrollView) findViewById(R.id.l_view);
//        emailTextView = (EditText) findViewById(R.id.nav_login_email_edit_text);
//        passwordTextView = (EditText) findViewById(R.id.nav_login_password_edit_text);
//        tvForgotPassword = (LinearLayout) findViewById(R.id.login_forget_password_text_view);
//
//        btnLogin = (Button) findViewById(R.id.nav_login_button);
//        btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(i);
//            }
//        });
    }
}