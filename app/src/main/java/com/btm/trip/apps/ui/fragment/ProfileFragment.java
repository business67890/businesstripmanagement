package com.btm.trip.apps.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.btm.trip.apps.R;

/**
 * Created by Owner on 12/3/2015.
 */
public class ProfileFragment extends Fragment {
    private Button settingButton;
    private Button signOutButton;
    private LinearLayout llEdit;
    private TextView fullNameTextView;
    private TextView usernameTextView;
    private TextView nameTextView;
    private TextView jointdateTextView;
    private TextView genderTextView;
    private TextView birthdateTextView;
    private TextView phoneTextView;
    private TextView locationTextView;
    private TextView userheighttextview;
    private TextView userweighttextview;
    private TextView userbloodtextview;
    private ImageView profileimage;
    private LinearLayout layoutloading;
    private Button refresh;
    private RelativeLayout l_view;
    final public static String AVATAR_IMAGE = "avatarImage";
    String imageurl = null;
    String currentLanguage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_view_profile, container, false);
        currentLanguage = getResources().getConfiguration().locale.getLanguage();
        currentLanguage = (currentLanguage.equals("en") || currentLanguage.equals("EN")) ? "en" : "id";
        settingButton = (Button) layout.findViewById(R.id.profile_view_setting_button);
        signOutButton = (Button) layout.findViewById(R.id.profile_view_sign_out_button);
        llEdit = (LinearLayout) layout.findViewById(R.id.llEdit);
        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do nothing
            }
        });

        TextView textView = (TextView) layout.findViewById(R.id.profile_view_edit_text_view);
        SpannableString content = new SpannableString("" + textView.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do nothing
            }
        });

        fullNameTextView = (TextView) layout.findViewById(R.id.user_profile_fullname_text_view);
        usernameTextView = (TextView) layout.findViewById(R.id.user_profile_username_text_view);
        jointdateTextView = (TextView) layout.findViewById(R.id.user_profile_join_date_text_view);
        nameTextView = (TextView) layout.findViewById(R.id.user_profile_name_text_view);
        genderTextView = (TextView) layout.findViewById(R.id.user_profile_gender_text_view);
        birthdateTextView = (TextView) layout.findViewById(R.id.user_profile_birthdate_text_view);
        phoneTextView = (TextView) layout.findViewById(R.id.user_profile_phone_text_view);
        locationTextView = (TextView) layout.findViewById(R.id.user_profile_location_text_view);
        userheighttextview = (TextView) layout.findViewById(R.id.user_profile_height_text_view);
        userweighttextview = (TextView) layout.findViewById(R.id.user_profile_weight_text_view);
        userbloodtextview = (TextView) layout.findViewById(R.id.user_profile_blood_type_text_view);
        profileimage = (ImageView) layout.findViewById(R.id.profileimage);
        l_view = (RelativeLayout) layout.findViewById(R.id.l_view);
        return layout;
    }
}
